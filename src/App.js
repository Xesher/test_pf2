import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import UserShow from './UserShow';
import Employees from './Employees';
import store from './config/store';
import './App.css';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Employees}/>
            <Route path="/show/:id/" component={UserShow}/>
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;