import React, { Component } from 'react';
import { connect } from 'react-redux';
import employeesData from './employees.json';
import rolesData from './roles.json';
import { loadInitValues, loadInitRoles } from './modules/actions';
import UserList from './UserList';
import Filter from "./Filter";

class Employees extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(loadInitValues(employeesData));
        dispatch(loadInitRoles(rolesData));
    }

    render() {
        const { data: { users } } = this.props;

        if (users.length === 0) {
            return 'Загрузка';
        }

        return (
            <div className="employees">
                <Filter users={employeesData} />
                <UserList users={users} />
            </div>
        );
    }
}

Employees = connect((state) => ({
    data: state.employees,
}))(Employees);

export default Employees;
