import { configureStore } from './../modules/store';

const store = configureStore();

export default store;