import React from 'react';
import { Link } from 'react-router-dom';

const UserItem = ({ user: { id, name, role, phone } }) => {
    return (
        <Link to={`show/${id}`} className="user-item">
          <div className="user-item-name">
              {name}
          </div>
          <div className="user-item-role">
              {role}
          </div>
            <div className="user-item-phone">
              {phone}
          </div>
        </Link>
    )
};

export default UserItem;
