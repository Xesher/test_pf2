import React, { Component } from 'react';
import { connect } from 'react-redux';
import employeesData from './employees.json';
import { loadInitValues } from './modules/actions';

class UserShow extends Component {
  componentDidMount() {
      const { dispatch } = this.props;
      dispatch(loadInitValues(employeesData));
  }

  render() {
  const { data: { users }, match: { params: { id } } } = this.props;
  const user = users.filter((user) => user.id === parseInt(id, 10))[0];

  if (!user) {
    return 'Загрузка';
  }

  return (user && (
      <div className="user-show">
        <div className="user-show-id">
            {user.id}
        </div>
        <div className="user-show-name">
            {user.name}
        </div>
        <div className="user-show-phone">
            {user.phone}
        </div>
      </div>
      )
    )
  }
};

UserShow = connect((state) => ({
    data: state.employees,
}))(UserShow);

export default UserShow;
