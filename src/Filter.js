import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { connect } from 'react-redux';
import { dataSortBy, addFilter, loadInitValues } from './modules/actions';

let Filter = ({ dispatch, roles, filter, users }) => {

    const sortBy = (key, sort) => {
        dispatch(dataSortBy(key, sort));
    };

    const onChangeFilterRole = (value) => {
        dispatch(addFilter('role', value));
        dispatch(loadInitValues(users));
    };

    return (
        <div className="user-filter">
            <div className="user-filter-inner">
                <div onClick={() => sortBy('name', 'asc')}>Сортировать по имени asc</div>
                <div onClick={() => sortBy('name', 'desc')}>Сортировать по имени desc</div>

                <div onClick={() => sortBy('birthday', 'asc')}>Сортировать по ДР asc</div>
                <div onClick={() => sortBy('birthday', 'desc')}>Сортировать по ДР desc</div>

                <Select
                    name="form-filter-role"
                    value={filter.role}
                    options={roles}
                    onChange={onChangeFilterRole}
                />
            </div>
        </div>
    )
};

Filter = connect((state) => ({
    filter: state.employees.filter,
    roles: state.employees.roles,
}))(Filter);

export default Filter;
