import  * as types  from '../const';
import sortBy from 'lodash/sortBy';

const initState = {
    users: [],
    roles: [],
    filter: {
        role: '',
        status: null,
    },
    key: null,
    sort: null,
};

const reducer = (state = initState, action) => {
  switch (action.type) {
      case types.EMPLOYEES_LOAD_INIT:
          let userList;
          if (state.filter.role && state.filter.role.value) {
              userList = action.data.filter((user) => user.role === state.filter.role.value);
          } else {
              userList = action.data;
          }
          return Object.assign({}, state, {
              users: userList
          });
      case types.ROLES_LOAD_INIT:
          return Object.assign({}, state, {
              roles: action.data
          });
      case types.ADD_FILTER:
         return Object.assign({}, state, {
              filter: {
                  [action.key]: action.value
              }
          });

      case types.EMPLOYEES_SORT_BY:
          let users;
          state.key = action.key;
          state.sort = action.sort;

          switch (action.sort) {
              case 'asc':
                  users = sortBy(state.users, [action.key]);
                  break;
              case 'desc':
                  users = sortBy(state.users, [action.key]).reverse();
                  break;
              default:
                  break;
          }

          return Object.assign({}, state, {
              users,
          });
    default:
      return state;
  }
};

export default reducer;