import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from './reducers/combined';
import thunk from 'redux-thunk';

// eslint-disable-next-line import/no-mutable-exports
export let store;

const devTools = (process.env.NODE_ENV === 'development' && window.devToolsExtension)
  ? window.devToolsExtension()
  : f => f;

export function configureStore() {
  store = createStore(
    rootReducer,
    compose(
      applyMiddleware(...[thunk]),
      devTools
    )
  );

  return store;
}