import { EMPLOYEES_LOAD_INIT, EMPLOYEES_SORT_BY, ROLES_LOAD_INIT, ADD_FILTER } from '../const';

export const loadInitValues = (data) => ({ type: EMPLOYEES_LOAD_INIT, data });

export const loadInitRoles = (data) => ({ type: ROLES_LOAD_INIT, data });

export const addFilter = (key, value) => ({ type: ADD_FILTER, key, value });

export const dataSortBy = (key, sort) => ({ type: EMPLOYEES_SORT_BY, key, sort });
