/* eslint-disable import/prefer-default-export */

/**
 * Actions
 * @type {string}
 */
export const EMPLOYEES_LOAD_INIT = 'EMPLOYEES_LOAD_INIT';
export const EMPLOYEES_SORT_BY = 'EMPLOYEES_SORT_BY';

export const ROLES_LOAD_INIT = 'ROLES_LOAD_INIT';

export const ADD_FILTER = 'ADD_FILTER';
