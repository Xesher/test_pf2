import React from 'react';
import UserItem from './UserItem';

const UserList = ({ users }) => {
    if (users.length === 0) {
      return 'Загрузка';
    }

    return (
        <div className="user-list">
            {users && users.map((user, index) => <UserItem key={index} user={user}/>)}
        </div>
    );
};

export default UserList;
